" vim-plug {{{
call plug#begin('~/.config/nvim/plugged')

" To get more information about these plugins, take a look at README.md

" vim-sensible
Plug 'tpope/vim-sensible'

" vim-fugitive depends on unite.vim
Plug 'Shougo/unite.vim' | Plug 'tpope/vim-fugitive'

" NERDTree, replacement for vim's browser
Plug 'scrooloose/nerdtree'

" base16 colors
Plug 'chriskempson/base16-vim'

" vim-airline and vim-airline themes
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Vim-bufferline
Plug 'bling/vim-bufferline'

" Syntastic
"Plug 'scrooloose/syntastic'

call plug#end()
" }}}

" Set options {{{
set incsearch
set hlsearch
set ignorecase
set smartcase
set wildmenu
set lazyredraw
set showmatch
set nocp
set number
set cursorline
set so=2
set expandtab
set shiftwidth=4
set softtabstop=4
set ai
set smartindent
"set backupdir=~/.config/nvim/backups
"set directory=~/.config/nvim/swaps

" Set cursor as blinking in insert mode, and a square in normal mode
let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

if exists("%undodir")
	set undodir=~/.config/nvim/undo
endif

" Folding behaviour
set foldlevelstart=10
set foldnestmax=10
set foldmethod=indent

" Enable syntax highlighting
syntax enable

filetype on
filetype indent on

" Automatic commands
augroup comment
	autocmd!
	autocmd FileType javascript nnoremap <buffer> <localleader>c I//<esc>
	autocmd FileType python nnoremap <buffer> <localleader>c I#<esc>
	autocmd BufNewFile,BufRead *.vim nnoremap <buffer> <localleader>c I"<esc>
	autocmd BufNewFile,BufRead *.md setlocal filetype=markdown
	autocmd bufwritepost vimrc source $MYVIMRC
augroup END

" Default file explorer to VimFiler
let g:vimfiler_as_default_explorer = 1
" }}}

" Mappings {{{
" Set <leader> and <localleader>
nnoremap "'" <nop>
let mapleader = "'"
let g:mapleader = "'"
let maplocalleader = ","

" Faster save and exit
nnoremap <leader>w :wq<cr>

" Toggle relativenumber
nnoremap <leader>r :set relativenumber!<cr>

" Move a line of text using ALT+[jk] or Command+[jk] on mac
nmap <M-k> mz:m-2<cr>`z
nmap <M-j> mz:m+<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

" Treat wrapped lines as individual when moving vertically
nnoremap j gj
nnoremap k gk

" Map <Space> to search and Ctrl+<Space> to search backwards
"nnoremap <space> /
"nnoremap <c-space> ?

" Move between windows in normal mode
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l

" Close buffer
nnoremap <leader>bd :Bclose<cr>:tabclose<cr>gT

" Useful mappings for managing tabs
"map <leader>tn :tabnew<cr>
"map <leader>to :tabonly<cr>
"map <leader>tc :tabclose<cr>
"map <leader>tm :tabmove 
"map <leader>t<leader> :tabnext

" Space toggles a fold
nnoremap <space> za

" The Silver Searcher mapping
nnoremap <leader>a :Ag

" NERDTree mapping
map <C-n> :NERDTreeToggle<CR>

" }}}

" Colorscheme {{{
" Vim Solarized theme
" Uncomment the following lines if you wish not to use the solarized
" colorscheme
set background=dark
"colorscheme base16-irblack
" }}}

" Vim-airline {{{
" Powerline symbols for vim-airline
let g:airline_powerline_fonts = 1

" Set vim-airline theme
" Comment the following line if you wish not to use the solarized
" colorscheme
"let g:airline_theme="solarized"
let g:airline_theme="dark"

" }}}

" Vim-bufferline {{{

" Stop bufferline echoing to command bar
let g:bufferline_echo = 0

" }}}

" Enable folding in init.vim to make editing init.vim easier
" Use "za" or "<space>" to open/close a tab
" vim:foldmethod=marker:foldlevel=0
