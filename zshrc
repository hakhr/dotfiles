#
# My zshrc, 
# heavily based on https://github.com/rotsix/dotfiles
#

source $HOME/.zsh.d/main
source $HOME/.zsh.d/variables
source $HOME/.zsh.d/alias
source $HOME/.zsh.d/prompt_oneline
source $HOME/.zsh.d/functions
source $HOME/.zsh.d/history

source $HOME/.dotfiles/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source $HOME/.dotfiles/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

source $HOME/.zsh.d/syntax
